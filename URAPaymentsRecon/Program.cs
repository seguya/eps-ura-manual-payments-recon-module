﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;

namespace URAPaymentsRecon
{
    class Program
    {

        private static string connString = "", sql = "SELECT IncidentID, TicketNo, DriverName, PermitNumber, Date, Time, PlaceOfOffense, CarRegNo, CarMake, CarColor, CarOwner, OwnerAddress, OwnerTelephone, OfficerID, OfficerName, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, Gender, CategoryID, Chasis, Station, offenceID FROM eps_tickets";
       private static string sSheetName = "Sheet 1", ServerNameSrc = "192.168.16.34", DBNameSrc = "EPS_NEW";
        //private static string sSheetName = "Sheet 1", ServerNameSrc = "ABUTRIKAR", DBNameSrc = "EPS_NEW";
        private static string TicketNo, DriverName, PermitNumber, Time, PlaceOfOffense, CarRegNo, CarMake, CarColor, CarOwner, OwnerAddress, OwnerTelephone, OfficerID, OfficerName, CreatedBy, UpdatedBy, Gender, CategoryID, Chasis;
        private static int Station, offenceID;
        private static DateTime Date;
        private static DateTime CreatedOn, UpdatedOn;

        static void Main(string[] args)
        {
            Console.WriteLine("<< URA PAYMENTS RECONCILIATION & DATA UPDATE >>");
            Console.WriteLine("");

            // Option 1: Update Piloting Server
            Console.WriteLine("# Enter 1 For DATA UPDATE OR 2 for PAYMENTS RECONCILIATION");
            string option = Console.ReadLine();

            Console.WriteLine("# Enter Server Name:");
            string ServerName = Console.ReadLine();

            Console.WriteLine("# Enter Datatabase Name:");
            string DBName = Console.ReadLine();

            Console.WriteLine("Authentication Type (Enter 1 for Windows Auth or 2 Otherwise)");
            string AuthType = Console.ReadLine();

            // Connect to Server
            if (ServerName.Equals(""))
                ServerName = "ABUTRIKAR";
            if (DBName.Equals(""))
                DBName = "EPS_NEW";

            if (AuthType.Equals("1"))
            {
                connString = "Server=" + ServerName + ";Database=" + DBName + ";Trusted_Connection=True;";
            }
            else
            {
                Console.WriteLine("# Enter Server Password:");
                string ServerPassword = Console.ReadLine();

                if (ServerPassword.Equals(""))
                    ServerPassword = "pa$$";

                connString = "Server=" + ServerName + ";Database=" + DBName + ";User Id=sa; Password=" + ServerPassword + ";";

            }

            if (option.Equals("1"))
            {
                Console.WriteLine("# STATUS: Entered DATA UPDATE Section");
                string connStringSrc = "Server=" + ServerNameSrc + ";Database=" + DBNameSrc + ";User Id=sps; pwd=sps@123;";

                Console.WriteLine("# Testing Live server connection....");
                if (!IsServerConnected(connStringSrc))
                {
                    Console.WriteLine("# Connection Error: Connecting to the Live Server failed.");
                }
                else
                {
                    // Get last IncidentID
                    Console.WriteLine("# Reading last update status");
                    string maxIncidentId = "select max(incidentid) from eps_tickets";
                    decimal LastIncidentId = decimal.Parse(GetDataSet(connString, maxIncidentId).Tables[0].Rows[0][0].ToString());

                    Console.WriteLine("# Info: Last record # " + LastIncidentId.ToString());

                    sql += " where IncidentID > " + LastIncidentId;
                    DataTable dt = GetDataSet(connStringSrc, sql).Tables[0];

                    if (dt.Rows.Count == 0)
                    {
                        Console.WriteLine("# No NEW Data to Fetch/Upload ");
                    }
                    else
                    {
                        int i = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            TicketNo = dr["TicketNo"].ToString();
                            DriverName = dr["DriverName"].ToString();
                            PermitNumber = dr["PermitNumber"].ToString();
                            Date = Convert.ToDateTime(dr["Date"].ToString());
                            Time = dr["Time"].ToString();
                            PlaceOfOffense = dr["PlaceOfOffense"].ToString();
                            CarRegNo = dr["CarRegNo"].ToString();
                            CarMake = dr["CarMake"].ToString();
                            CarColor = dr["CarColor"].ToString();
                            CarOwner = dr["CarOwner"].ToString();
                            OwnerAddress = dr["OwnerAddress"].ToString();
                            OwnerTelephone = dr["OwnerTelephone"].ToString();
                            OfficerID = dr["OfficerID"].ToString();
                            OfficerName = dr["OfficerName"].ToString();
                            CreatedOn = Convert.ToDateTime(dr["CreatedOn"].ToString());
                            CreatedBy = dr["CreatedBy"].ToString();
                            UpdatedOn = Convert.ToDateTime(dr["UpdatedOn"].ToString());
                            UpdatedBy = dr["UpdatedBy"].ToString();
                            Gender = dr["Gender"].ToString();
                            CategoryID = dr["CategoryID"].ToString();
                            Chasis = dr["Chasis"].ToString();
                            Station = int.Parse(dr["Station"].ToString());
                            offenceID = int.Parse(dr["offenceID"].ToString());

                            bool moved = MoveTicket(TicketNo, DriverName, PermitNumber, Date, Time, PlaceOfOffense, CarRegNo, CarMake, CarColor, CarOwner, OwnerAddress, OwnerTelephone, OfficerID, OfficerName, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, Gender, CategoryID, Chasis, Station, offenceID);

                            if (moved)
                            {
                                Console.WriteLine("# Ticket " + TicketNo + " uploaded");
                                i++;
                            }

                            Console.WriteLine("# " + i.ToString() + " Tickets uploaded");
                        }
                    }
                    
                }

            }
            else
            {
                Console.WriteLine("# STATUS: Entered PAYMENTS RECONCILIATION Section");
                try
                {
                    

                    Console.WriteLine("# Enter File Location:");
                    string FileLocation = Console.ReadLine();

                    Console.WriteLine("# Enter Excel Sheet Name e.g Sheet 1:");
                    sSheetName = Console.ReadLine();

                    if (FileLocation.Equals(""))
                        FileLocation = @"C:\Users\Ahmed\Desktop\rec.xls";
                    if (sSheetName.Equals(""))
                        sSheetName = "Sheet 1";

                    Console.WriteLine("# Connecting to " + ServerName + "........");

                    if (!IsServerConnected(connString))
                    {
                        Console.WriteLine("# Connection Error: Connecting to the Server failed.");
                    }
                    else
                    {
                        Console.WriteLine("# Verifying file ");
                        FileLocation = FileLocation.Replace("\"", "");

                        if (!File.Exists(FileLocation))
                        {
                            Console.WriteLine("# Error: Invalid file path");
                        }
                        else if (Path.GetExtension(FileLocation) != ".xls" && Path.GetExtension(FileLocation) != ".xlsx")
                        {
                            Console.WriteLine("# Error: Invalid file selected");
                        }
                        else
                        {
                            Console.WriteLine("# Reading data file.... ");
                            Stopwatch stopWatch = new Stopwatch();
                            stopWatch.Start();

                            DataTable dt = ReadExcelFile(FileLocation, "Yes");

                            Console.WriteLine("# No of records to reconcile: " + dt.Rows.Count.ToString());

                            DateTime PAYMENT_DATE;
                            decimal AMOUNT;
                            string TICKET_NUMBER, DRIVER_NAME, PRN_NO, CAR_REGISTRATION_NUMBER, OFFENCE_CODE, OFFENCE_DESC;
                            int i = 0;

                            Console.WriteLine("# Posting payments.... ");
                            foreach (DataRow dr in dt.Rows)
                            {
                                TICKET_NUMBER = dr[0].ToString().Trim();
                                DRIVER_NAME = dr[1].ToString();
                                if (!TICKET_NUMBER.Equals(""))
                                {

                                    PRN_NO = dr[2].ToString();
                                    Console.WriteLine("1" + PRN_NO);
                                    PAYMENT_DATE = Convert.ToDateTime(dr[3].ToString());
                                    Console.WriteLine("2" + PAYMENT_DATE);
                                    AMOUNT = Convert.ToDecimal(dr[4].ToString());
                                    Console.WriteLine("3" + AMOUNT);
                                    CAR_REGISTRATION_NUMBER = dr[5].ToString();
                                    Console.WriteLine("4" + CAR_REGISTRATION_NUMBER);
                                    OFFENCE_CODE = dr[6].ToString();
                                    Console.WriteLine("5"+ OFFENCE_CODE);
                                    OFFENCE_DESC = dr[7].ToString();
                                    Console.WriteLine("6" + OFFENCE_DESC);

                                    bool reconciled = ReconcilePayment(TICKET_NUMBER, DRIVER_NAME, PRN_NO, PAYMENT_DATE, AMOUNT, CAR_REGISTRATION_NUMBER, OFFENCE_CODE, OFFENCE_DESC);
                                    Console.WriteLine("7");
                                    if (reconciled)
                                    {
                                        Console.WriteLine("# " + sSheetName + "Posted " + TICKET_NUMBER);
                                        i++;
                                    }

                                    Console.WriteLine("# No " + i.ToString());
                                }
                                else
                                {
                                    Console.WriteLine("# Unavailable: " + DRIVER_NAME);
                                }
                            }
                            stopWatch.Stop();
                            TimeSpan ts = stopWatch.Elapsed;
                            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

                            Console.WriteLine("# Done posting payments.... ");
                            Console.WriteLine("# " + i.ToString() + " payments reconciled in " + elapsedTime + " .");
                        }

                    }

                    Console.WriteLine("# Press <<Enter>> to Exit");
                    string input = Console.ReadLine();
                    if (input.ToLower().Equals("r"))
                    {
                        //// start new process
                        //System.Diagnostics.Process.Start(Environment.GetCommandLineArgs()[0], Environment.GetCommandLineArgs()[1]);

                        //// close current process
                        //Environment.Exit(0);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("# Error: " + ex.Message + " Details: " + ex.InnerException.StackTrace);
                }
            }

            Console.WriteLine("# Press <Enter> to Exit");
            Console.ReadLine();

        }

        private static bool ReconcilePayment(string TICKET_NUMBER, string DRIVER_NAME, string PRN_NO, DateTime PAYMENT_DATE, decimal AMOUNT, string CAR_REGISTRATION_NUMBER, string OFFENCE_CODE, string OFFENCE_DESC)
        {
            int i = 0;
            Console.WriteLine("cString "+connString);
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_reconcile_payment", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        Console.WriteLine("# Posting " + TICKET_NUMBER + " at " + DateTime.Now.ToShortTimeString());

                        cmd.Parameters.Add("@TICKET_NUMBER", SqlDbType.VarChar).Value = TICKET_NUMBER;
                        cmd.Parameters.Add("@DRIVER_NAME", SqlDbType.VarChar).Value = DRIVER_NAME;
                        cmd.Parameters.Add("@PRN_NO", SqlDbType.VarChar).Value = PRN_NO;
                        cmd.Parameters.Add("@PAYMENT_DATE", SqlDbType.Date).Value = PAYMENT_DATE;
                        cmd.Parameters.Add("@AMOUNT", SqlDbType.Decimal).Value = AMOUNT;
                        cmd.Parameters.Add("@CAR_REGISTRATION_NUMBER", SqlDbType.VarChar).Value = CAR_REGISTRATION_NUMBER;
                        cmd.Parameters.Add("@OFFENCE_CODE", SqlDbType.VarChar).Value = OFFENCE_CODE;
                        cmd.Parameters.Add("@OFFENCE_DESC", SqlDbType.VarChar).Value = OFFENCE_DESC;
                        con.Open();
                        // cmd.CommandTimeout = 0;
                        i = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            if (i == 0)
                return false;
            else
                return true;
        }

        private static bool MoveTicket(string TicketNo, string DriverName, string PermitNumber, DateTime Date, string Time, string PlaceOfOffense, string CarRegNo, string CarMake, string CarColor, string CarOwner, string OwnerAddress, string OwnerTelephone, string OfficerID, string OfficerName, DateTime CreatedOn, string CreatedBy, DateTime UpdatedOn, string UpdatedBy, string Gender, string CategoryID, string Chasis, int Station, int offenceID)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(connString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_move_tickets", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    Console.WriteLine("# Uploading " + TicketNo + " at " + DateTime.Now.ToShortTimeString());

                    // TicketNo, DriverName, PermitNumber, Date, Time, PlaceOfOffense, CarRegNo, CarMake, CarColor, CarOwner, OwnerAddress, 
                    // OwnerTelephone, OfficerID, OfficerName, CreatedOn, CreatedBy, UpdatedOn, 
                    // UpdatedBy, Gender, CategoryID, Chasis, Station, offenceID
                    cmd.Parameters.Add("@TicketNo", SqlDbType.VarChar).Value = TicketNo;
                    cmd.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = DriverName;
                    cmd.Parameters.Add("@PermitNumber", SqlDbType.VarChar).Value = PermitNumber;
                    cmd.Parameters.Add("@Date", SqlDbType.Date).Value = Date;
                    cmd.Parameters.Add("@Time", SqlDbType.VarChar).Value = Time;
                    cmd.Parameters.Add("@PlaceOfOffense", SqlDbType.VarChar).Value = PlaceOfOffense;
                    cmd.Parameters.Add("@CarRegNo", SqlDbType.VarChar).Value = CarRegNo;
                    cmd.Parameters.Add("@CarMake", SqlDbType.VarChar).Value = CarMake;
                    cmd.Parameters.Add("@CarColor", SqlDbType.VarChar).Value = CarColor;
                    cmd.Parameters.Add("@CarOwner", SqlDbType.VarChar).Value = CarOwner;
                    cmd.Parameters.Add("@OwnerAddress", SqlDbType.VarChar).Value = OwnerAddress;
                    cmd.Parameters.Add("@OwnerTelephone", SqlDbType.VarChar).Value = OwnerTelephone;
                    cmd.Parameters.Add("@OfficerID", SqlDbType.VarChar).Value = OfficerID;
                    cmd.Parameters.Add("@OfficerName", SqlDbType.VarChar).Value = OfficerName;
                    cmd.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = CreatedOn;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.VarChar).Value = CreatedBy;
                    cmd.Parameters.Add("@UpdatedOn", SqlDbType.DateTime).Value = UpdatedOn;
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.VarChar).Value = UpdatedBy;
                    cmd.Parameters.Add("@Gender", SqlDbType.VarChar).Value = Gender;
                    cmd.Parameters.Add("@CategoryID", SqlDbType.VarChar).Value = CategoryID;
                    cmd.Parameters.Add("@Chasis", SqlDbType.VarChar).Value = Chasis;
                    cmd.Parameters.Add("@Station", SqlDbType.Int).Value = Station;
                    cmd.Parameters.Add("@offenceID", SqlDbType.Int).Value = offenceID;

                    con.Open();
                    // cmd.CommandTimeout = 0;
                    i = cmd.ExecuteNonQuery();
                }
            }

            if (i == 0)
                return false;
            else
                return true;
        }


        private static DataTable ReadExcelFile(string FileLocation, string HDR)
        {
            
            OleDbConnection MyConnection;
            DataSet DtSet;
            System.Data.OleDb.OleDbDataAdapter MyCommand;

            // strHeader7 = (hdr7) ? "Yes" : "No";
            MyConnection = new System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileLocation + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"");
            MyCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [" + sSheetName + "$]", MyConnection);
            MyCommand.TableMappings.Add("Table", "TestTable");
            DtSet = new System.Data.DataSet();
            MyCommand.Fill(DtSet);
            MyConnection.Close();

            return DtSet.Tables[0];
        }

        /// <summary>
        /// Test that the server is connected
        /// </summary>
        /// <param name="connectionString">The connection string</param>
        /// <returns>true if the connection is opened</returns>
        private static bool IsServerConnected(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }



        public static DataSet GetDataSet(string ConnectionString, string SQL)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = SQL;
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();

            conn.Open();
            da.Fill(ds);
            conn.Close();

            return ds;
        }


    }
}
